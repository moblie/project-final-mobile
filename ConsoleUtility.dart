import 'dart:io';

/**
 * เครื่องมือสำหรับจัดการคอนโซลหรือเทอร์มินอล
 */
class ConsoleUtility {
  // ย้ายเคอร์เซอร์ไปยังตำแหน่งที่กำหนด
  void setPosition(int r, int c) => stdout.write("\x1B[$r;${c}H");

  // รีเซ็ตคอนโซลหรือเทอร์มินอล และย้ายเคอร์เซอร์ไปยังจุดเริ่มต้น
  void clear() => stdout.write("\x1B[2J\x1B[0;0H");
}

class CursorUtility extends ConsoleUtility {}
