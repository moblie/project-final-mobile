import 'Seed.dart';

// class Potato extends Seed {
//   String get _name => "Potato";
//   int get _maxDay => 10;
//   int get _sellPrice => 20;
//   int get _buyPrice => 15;
// }

class RiceSeed extends Seed {
  RiceSeed() : super("Rice seed", 5, 1, 7, ItemType.Seed, Rice());
}

class Rice extends Seed {
  Rice() : super("Rice", 5, 10, 0, ItemType.Product, null);
}

class PotatoSeed extends Seed {
  PotatoSeed() : super("Potato seed", 5, 8, 15, ItemType.Seed, Potato());
}

class Potato extends Seed {
  Potato() : super("Potato", 5, 20, 0, ItemType.Product, null);
}

class CustomSeed extends Seed {
  CustomSeed(
      String name, int maxDay, int sellPrice, int buyPrice, Seed? product)
      : super(name, maxDay, sellPrice, buyPrice, ItemType.Seed, product);
}

class Shop {
  List<Seed> seedList = [
    // CustomSeed("Potato", 10, 20, 15),
    PotatoSeed(),
    RiceSeed(),
  ];
}
