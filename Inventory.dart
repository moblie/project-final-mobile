import 'Seed.dart';

class Item {
  late int stack;
  late String name;
  late Seed item;
}

class Inventory {
  List<Item> _items = [];

  List<Item> get items => _items;

  void addItem(Seed item) {
    Item _item = Item();
    _item.stack = 1;
    _item.name = item.name;
    _item.item = item;
    int index = this._items.indexWhere((item) => item.name == _item.name);
    if (index < 0) {
      _items.add(_item);
    } else {
      this._items[index].stack++;
    }
  }

  void removeItem(int index) {
    if (this._items[index].stack > 1)
      this._items[index].stack--;
    else
      this._items.removeAt(index);
  }
}

// class CustomSeed extends Seed {
//   CustomSeed(String name, int maxDay, int sellPrice, int buyPrice)
//       : super(name, maxDay, sellPrice, buyPrice);
// }

// void main(List<String> args) {
//   List<Item> _items = [];
//   Item _item1 = Item();
//   _item1.stack = 1;
//   _item1.name = "TEST";
//   _item1.item = CustomSeed("Potato", 10, 20, 15);
//   _items.add(_item1);

//   Item _item2 = Item();
//   _item2.stack = 1;
//   _item2.name = "TEST2";
//   _item2.item = CustomSeed("Potato", 10, 20, 15);
//   _items.add(_item2);

//   Item _itemss = Item();
//   _itemss.name = "TEST2w";
//   int index = _items.indexWhere((item) => item.name == _itemss.name);
//   print(_items[0].name);
//   print(_items[1].name);
//   print(index);
// }
