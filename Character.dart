enum Gender { Male, Female, Null }

class Character {
  String _name = "";
  Gender _gender = Gender.Null;
  int _stamina = 10;
  int _maxStamina = 10;
  int _coin = 100;

  Character(String name, Gender gender) {
    this._name = name;
    this._gender = gender;
  }

  String name() => this._name;
  Gender gender() => this._gender;
  int stamina() => this._stamina;
  int maxStamina() => this._maxStamina;
  // int coin() => this._coin;
  int get coin => this._coin;

  void useCoin(int coin) {
    this._coin = this._coin - coin;
  }

  void addCoin(int coin) {
    this._coin = this._coin + coin;
  }

  void setCoin(int coin) {
    this._coin = coin;
  }

  void applyDay() {
    this._stamina = this._maxStamina;
  }

  /**
   * เพิ่ม Stamina โดยกำหนดค่าที่จะเพิ่มด้วย
   */
  void addSP(int SP) {
    /**
     * นำค่าที่ได้มาไปบวกกับ Stamina ปัจจุบัน
     * แล้วนำไปเก็บไว้ใน _stamina
     */
    int _tempStamina = this._stamina + SP;

    /**
     * ตรวจสอบว่า _tempStamina มีค่ามากกว่าค่าสูงสุดใน maxStamina หรือไม่
     */
    if (_tempStamina > this._maxStamina)
      // หากสูงกว่า ให้ใช้ค่าสูงสุด
      this._stamina = this._maxStamina;
    else
      // หากน้อยกว่าหรือเท่ากัน ให้ใช้ค่าที่คำนวณมา
      this._stamina = _tempStamina;
  }

  /**
   * ใช้งาน Stamina โดยกำหนดค่าที่จะใช้ด้วย 
   * มีการส่งค่าว่าสามารถใช้ Stamina ได้ไหม กลับไปด้วย
   */
  bool useStamina(int SP) {
    // หากค่าที่รับมาน้อยกว่า 0 ให้ส่งค่ากลับเป็น false
    if (SP < 0) return false;
    /**
     * นำค่าที่ได้มาไปลบกับ Stamina ปัจจุบัน
     * แล้วนำไปเก็บไว้ใน _tempStamina
     */
    int _tempStamina = this._stamina - SP;
    /**
     * ตรวจสอบว่า _stamina มีค่าน้อยกว่า 0 หรือไม่
     */
    if (_tempStamina < 0)
      // ถ้าใช่ ให้ส่งค่ากลับเป็น false
      return false;
    else {
      /**
       * ถ้าไม่ ให้อัพเดทค่า Stamina ปัจจุบันตามที่ได้คำนวณไว้ใน _tempStamina
       * และส่งค่ากลับเป็น true
       */
      this._stamina = _tempStamina;
      return true;
    }
  }
}
