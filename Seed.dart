enum ItemType {
  Seed,
  Product,
}

abstract class Seed {
  late String _name;
  late int _maxDay;
  late int _sellPrice;
  late int _buyPrice;
  late ItemType _type;
  late Seed? _product;

  int _state = 0;
  bool _water = false;
  bool _growStatus = false;
  bool _canHarvest = false;

  String get name => _name;
  int get buyPrice => _buyPrice;
  int get sellPrice => _sellPrice;
  Seed? get product => _product;
  int get state => _state;
  int get maxDay => _maxDay;
  bool get water => _water;
  bool get canHarvest => _canHarvest;
  ItemType get type => _type;

  Seed(String name, int maxDay, int sellPrice, int buyPrice, ItemType type,
      Seed? product) {
    this._name = name;
    this._maxDay = maxDay;
    this._sellPrice = sellPrice;
    this._buyPrice = buyPrice;
    this._type = type;
    this._product = product;
  }

  void applyDay() {
    int pushState = 0;
    if (this._water) pushState++;
    if (this._growStatus) pushState++;
    this._state = this._state + pushState;
    this._water = false;
    this._growStatus = false;
    this._canHarvest = this._state >= this._maxDay;
  }

  // void setMaxDay(int maxDay) => this._maxDay = maxDay;

  void setWater() {
    this._water = true;
  }

  void setGrow() {
    this._growStatus = true;
  }

  // bool get isProduct =>
}
