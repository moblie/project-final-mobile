import 'dart:io';
import 'dart:mirrors';
import 'Seed.dart';
import 'Shop.dart';
import 'Inventory.dart';
import 'Character.dart';
import 'ConsoleUtility.dart';

/**
 * ฟังก์ชั่นสำหรับดึงประเภทของตัวแปร
 */
getTypeName(dynamic obj) {
  return reflect(obj).type.reflectedType.toString();
}

/**
 * เมนูตอนเริ่มต้น
 */
class StartMenu {
  String show([String textHelp = ""]) {
    CursorUtility CursorUtil = CursorUtility();
    String textSelect = "Enter an option from 1-2: ";
    print('===================================');
    print('Welcome to My Game');
    print('-----------------------------------');
    print('[1] Start game');
    print('[2] Exit');
    print('-----------------------------------');
    print('');
    print('-----------------------------------');
    print(textHelp);
    print('===================================');
    CursorUtil.setPosition(7, 0);
    stdout.write(textSelect);
    String? selected = stdin.readLineSync(); // รับข้อมูล
    CursorUtil.setPosition(11, 0);
    return selected.toString();
  }
}

/**
 * เมนูตอนสร้างตัวละคร
 */
class CharacterCreateMenu {
  // เรียกใช้งาน CursorUtility
  CursorUtility _CursorUtil = CursorUtility();
  String _textHelp = "";
  // Character _character = Character();
  String? _name = "";
  String? _gender = "";

  String textSetName = "Enter name: ";
  String textSetGender = "Enter gender (male/female): ";
  void show() {
    while (_name == "") {
      _CursorUtil.clear();
      this.menu();
      _CursorUtil.setPosition(4, textSetName.length + 1);
      this._name = stdin.readLineSync();
      if (this._name == "") {
        this._textHelp = "Please enter character name.";
      } else {
        this._textHelp = "";
      }
    }
    while (this.isNotGender(this._gender.toString())) {
      _CursorUtil.clear();
      this.menu();
      _CursorUtil.setPosition(5, textSetGender.length + 1);
      this._gender = stdin.readLineSync();
      if (this._gender == "") {
        this._textHelp = "Please enter gender.";
      } else if (this.isNotGender(this._gender.toString())) {
        this._textHelp = "Please enter male or female only.";
        this._gender = "";
      } else {
        this._textHelp = "";
      }
    }
    _CursorUtil.setPosition(9, 0);
    // this.menu();
  }

  bool isNotGender(String value) {
    return (value == "" ||
        (value != "male" && value != "female" && value != "m" && value != "f"));
  }

  String getName() {
    return this._name.toString();
  }

  Gender getGender() {
    /**
     * if (this._gender == "male" || this._gender == "m") {
     *  return Gender.Male;
     * } else if (this._gender == "female" || this._gender == "f") {
     *  return Gender.Female;
     * } else {
     *  return Gender.Null;
     * }
     */
    return (this._gender == "male" || this._gender == "m")
        ? Gender.Male
        : (this._gender == "female" || this._gender == "f")
            ? Gender.Female
            : Gender.Null;
  }

  void menu() {
    print('===================================');
    print('Create Character');
    print('-----------------------------------');
    print("${this.textSetName}${this._name}");
    print("${this.textSetGender}${this._gender}");
    print('-----------------------------------');
    print(this._textHelp);
    print('===================================');
  }
}

class MainMenu {
  String? selected = "";
  String textSelect = "Enter an option from 1-2: ";
  String show([String textHelp = ""]) {
    selected = "";
    CursorUtility CursorUtil = CursorUtility();
    while (selected != "1" && selected != "2") {
      CursorUtil.clear();
      print('===================================');
      print('Main Menu');
      print('-----------------------------------');
      print("[1] Resume");
      print("[2] Quit");
      print('-----------------------------------');
      print('');
      print('-----------------------------------');
      print(textHelp);
      print('===================================');
      CursorUtil.setPosition(7, 0);
      stdout.write(textSelect);
      selected = stdin.readLineSync(); // รับข้อมูล
      CursorUtil.setPosition(11, 0);
      if (selected != "1" && selected != "2") {
        textHelp = "Please enter number 1-2 only.";
      }
    }

    return selected.toString();
  }
}

enum MenuState {
  main,
  plant,
  placeSeed,
  placeToPlot,
  inventory,
  shop,
  shopBuy,
  shopSell,
}

class MainMap {
  CursorUtility _cursorUtil = CursorUtility();
  MainMenu _mainMenu = MainMenu();
  Shop _shop = Shop();
  Inventory _inventory = Inventory();
  bool _run = true;
  late Character _character;
  int _day = 1;
  String _helpText = "";
  bool _useDevCmd = true;

  MainMap(Character character) {
    this._character = character;
  }
  MenuState menuState = MenuState.main;
  List<String> _menuList = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
  ];
  // CustomSeed("Test Seed", 2, 0, 0, null),
  // CustomSeed("Potato Seed", 2, 0, 0, Potato()),
  List<Seed?> plotList = [
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
  ];

  Seed? getPlotSeed(int index) {
    // length Start at 1
    // index  Start at 0
    if (index >= this.plotList.length)
      return null;
    else {
      Seed? seed = plotList[index];
      return seed;
    }
  }

  bool useStamina(int sp) {
    return this._character.useStamina(sp);
  }

  void plotHarvest(Seed seed, index) {
    Seed? product = seed.product;
    if (product != null) addSeedToInventory(product);
    this.plotList[index] = null;
  }

  void plotAction(int index) {
    index--;
    Seed? seed = getPlotSeed(index);
    if (seed != null) {
      if (seed.canHarvest) {
        if (useStamina(3))
          this.plotHarvest(seed, index);
        else
          this._helpText = "Stamina not enough";
      } else {
        if (useStamina(1))
          seed.setWater();
        else
          this._helpText = "Stamina not enough";
      }
    } else {
      this._helpText = "No seed in plot";
    }
  }

  void applyDay() {
    for (Seed? seed in this.plotList) {
      if (seed != null) seed.applyDay();
    }
    this._character.applyDay();
  }

  bool haveSeed(int index) {
    if (index >= _shop.seedList.length)
      return false;
    else
      return true;
  }

  Seed? getShopSeed(int index) {
    if (!this.haveSeed(index))
      return null;
    else {
      Seed seed = _shop.seedList[index];
      return seed;
    }
  }

  void addShopSeedToInventory(int index) {
    Seed? shopSeed = getShopSeed(index);
    if (shopSeed != null) addSeedToInventory(shopSeed);
  }

  void addSeedToInventory(Seed seed) {
    _inventory.addItem(seed);
  }

  String getShopBuyItem(int index) {
    Seed? seed = getShopSeed(index);
    if (seed != null)
      return "${seed.name} (-${seed.buyPrice.toString()} C)";
    else {
      return "";
    }
  }

  void buyItem(int index) {
    index = index - 1;
    if (!haveSeed(index))
      this._helpText = "No item in slot";
    else {
      Seed? shopSeed = getShopSeed(index);
      if (shopSeed == null) {
        this._helpText = "No item in slot";
        return;
      } else {
        if (this._character.coin - shopSeed.buyPrice < 0) {
          this._helpText = "Coin not enough";
        } else {
          this._helpText = "Buy ${index} ${shopSeed.buyPrice}";
          this._character.useCoin(shopSeed.buyPrice);
          this._inventory.addItem(shopSeed);
        }
      }
    }
  }

  void sellItem(int index) {
    index = index - 1;
    if (!haveItem(index))
      this._helpText = "No item in slot";
    else {
      Item? item = getInventoryItem(index);
      if (item == null) {
        this._helpText = "No item in slot";
        return;
      } else {
        Seed seed = item.item;
        this._helpText = "Selled ${index} +${seed.sellPrice} coin";
        this._character.addCoin(seed.sellPrice);
        this._inventory.removeItem(index);
      }
    }
  }

  bool haveItem(int index) {
    if (index >= _inventory.items.length)
      return false;
    else
      return true;
  }

  Item? getInventoryItem(int index) {
    if (index < 0 || index >= _inventory.items.length)
      return null;
    else {
      Item item = _inventory.items[index];
      return item;
    }
  }

  String getInventory(int index, [bool sell = false]) {
    if (index >= _inventory.items.length)
      return ""; //Empty
    else {
      Item item = _inventory.items[index];
      Seed seed = item.item;
      int stack = item.stack;
      int sellPrice = seed.sellPrice;
      if (sell)
        return "${item.name.toString()} (${stack} EA) (+${sellPrice} C)";
      else
        return "${item.name.toString()} (${stack} EA)";
    }
  }

  int? indexSeedSelected = null;
  List<Item> getSeedItemList() {
    List<Item> seedList = [];
    for (var item in this._inventory.items) {
      Seed seed = item.item;
      // seedList.add(seed);
      if (seed.type == ItemType.Seed) seedList.add(item);
    }
    return seedList;
  }

  void selectSeed(int index) {
    List<Item> seedList = getSeedItemList();
    if (index > seedList.length) {
      this._helpText = "No seed in slot";
    } else {
      indexSeedSelected = index - 1;
      this.menuState = MenuState.placeToPlot;
    }
  }

  void plotAction2(int index) {
    index--;
    Seed? seed = getPlotSeed(index);
    if (seed != null) {
      if (seed.canHarvest) {
        if (useStamina(3))
          this.plotHarvest(seed, index);
        else
          this._helpText = "Stamina not enough";
      } else {
        if (useStamina(1))
          seed.setWater();
        else
          this._helpText = "Stamina not enough";
      }
    } else {
      this._helpText = "No seed in plot";
    }
  }

  void seedToPlot(int index) {
    // this._helpText = "seedToPlot";
    index--;
    Seed? seed = getPlotSeed(index);
    List<Item> seedList = getSeedItemList();
    if (seed != null) {
      this._helpText = "Seed have already in this plot.";
    } else {
      int? inxSelect = this.indexSeedSelected;
      if (inxSelect != null) {
        if (inxSelect > seedList.length) {
          this._helpText =
              "No seed in inventory ${seedList.length}/${inxSelect}";
        } else {
          if (this.useStamina(2)) {
            Seed seed = seedList[inxSelect].item;
            int inx = 0;
            seedList.forEach((element) {
              if (inx == inxSelect)
                this.plotList[index] = CustomSeed(seed.name, seed.maxDay,
                    seed.sellPrice, seed.buyPrice, seed.product);
            });
            this._inventory.removeItem(inxSelect);
            this.menuState = MenuState.main;
          } else
            this._helpText = "Stamina not enough";
        }
      } else {
        this._helpText = "No seed is selected.";
      }
    }
  }

  List<String> getSeedList() {
    List<String> seedList = [];
    for (var item in this._inventory.items) {
      Seed seed = item.item;
      if (seed.type == ItemType.Seed)
        seedList.add("${seed.name} (${item.stack} EA)");
    }
    if (seedList.length < 10) {
      for (var i = seedList.length; i < 10; i++) {
        seedList.add("");
      }
    } else {
      seedList.sublist(0, 10);
    }
    return seedList;
  }

  // Seed? getPlantSeed(int index) {
  //   Seed? plant = this.plantList[index];
  //   if (plant != null) {
  //     return plant;
  //   } else
  //     return null;
  // }

  String getPlant(int index) {
    Seed? plant = getPlotSeed(index);
    if (plant != null) {
      String name = plant.name;
      // bool water = plant.water;
      return "${name}"; // ${water}
    } else
      return ""; //[Empty]
  }

  void run() {
    while (_run) {
      this._cursorUtil.clear();
      this.ui();
      _cursorUtil.setPosition(18, 5);
      String? command = stdin.readLineSync();
      this.checkCmd(command.toString());
      command = "";
    }
  }

  void updateMenuList(MenuState menu) {
    switch (menu) {
      case MenuState.main:
        this._helpText = "Select menu";
        this._menuList = [
          "Plant",
          "Place seed",
          "Inventory",
          "Open shop",
          "Next day",
          "",
          "",
          "",
          "",
          "Main Menu",
        ];
        break;
      case MenuState.plant:
        // this._helpText = "Select plant to interaction";
        this._menuList = [
          getPlant(0),
          getPlant(1),
          getPlant(2),
          getPlant(3),
          getPlant(4),
          getPlant(5),
          getPlant(6),
          getPlant(7),
          getPlant(8),
          "Back",
        ];
        break;
      case MenuState.placeSeed:
        // this._helpText = "Select seed";
        this._menuList = this.getSeedList();
        this._menuList[9] = "Back";
        break;

      case MenuState.placeToPlot:
        // this._helpText = "Select plot";
        this._menuList = [
          getPlant(0),
          getPlant(1),
          getPlant(2),
          getPlant(3),
          getPlant(4),
          getPlant(5),
          getPlant(6),
          getPlant(7),
          getPlant(8),
          "Back",
        ];
        break;
      case MenuState.inventory:
        this._helpText = "List items";
        this._menuList = [
          getInventory(0),
          getInventory(1),
          getInventory(2),
          getInventory(3),
          getInventory(4),
          getInventory(5),
          getInventory(6),
          getInventory(7),
          getInventory(8),
          "Back",
        ];
        break;
      case MenuState.shop:
        this._helpText = "Select action to shop";
        this._menuList = [
          "buy",
          "sell",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "Back",
        ];
        break;
      case MenuState.shopBuy:
        // this._helpText = "Select item to buy";
        this._menuList = [
          getShopBuyItem(0),
          getShopBuyItem(1),
          getShopBuyItem(2),
          getShopBuyItem(3),
          getShopBuyItem(4),
          getShopBuyItem(5),
          getShopBuyItem(6),
          getShopBuyItem(7),
          getShopBuyItem(8),
          "Back",
        ];
        break;
      case MenuState.shopSell:
        // this._helpText = "Select item to sell";
        this._menuList = [
          getInventory(0, true),
          getInventory(1, true),
          getInventory(2, true),
          getInventory(3, true),
          getInventory(4, true),
          getInventory(5, true),
          getInventory(6, true),
          getInventory(7, true),
          getInventory(8, true),
          "Back",
        ];
        break;
      default:
    }
  }

  void checkCmd(String cmd) {
    if (this.menuState == MenuState.main) {
      switch (cmd) {
        case "1":
          this.menuState = MenuState.plant;
          this._helpText = "Select plant to interaction";
          break;
        case "2":
          this.menuState = MenuState.placeSeed;
          this._helpText = "Select seed";
          break;
        case "3":
          this.menuState = MenuState.inventory;
          break;
        case "4":
          this.menuState = MenuState.shop;
          break;
        case "5":
          this.nextDay();
          break;
        case "0":
          this.openMainMenu();
          break;
        default:
          if (cmd.startsWith("/") == true) {
            this.devCmd(cmd.split(" "));
            // this._helpText = cmd;
          }
      }
    } else if (this.menuState == MenuState.plant) {
      this._helpText = "";
      List<String> indexMenu = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
      if (indexMenu.contains(cmd)) {
        this.plotAction(int.parse(cmd));
      } else if (cmd == "0") {
        this.menuState = MenuState.main;
      }
    } else if (this.menuState == MenuState.inventory) {
      switch (cmd) {
        case "0":
          this.menuState = MenuState.main;
          break;
        default:
      }
    } else if (this.menuState == MenuState.placeSeed) {
      // selectSeed
      this._helpText = "";
      List<String> indexMenu = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
      if (indexMenu.contains(cmd)) {
        // this._helpText = cmd;
        this.selectSeed(int.parse(cmd));
      } else if (cmd == "0") {
        this.menuState = MenuState.main;
      }
    } else if (this.menuState == MenuState.placeToPlot) {
      // selectSeed
      // this._helpText = "dawdawdawd";
      List<String> indexMenu = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
      if (indexMenu.contains(cmd)) {
        this.seedToPlot(int.parse(cmd));
      } else if (cmd == "0") {
        this.menuState = MenuState.main;
      }
    } else if (this.menuState == MenuState.shop) {
      switch (cmd) {
        case "1":
          this._helpText = "";
          this.menuState = MenuState.shopBuy;
          break;
        case "2":
          this._helpText = "";
          this.menuState = MenuState.shopSell;
          break;
        case "0":
          this.menuState = MenuState.main;
          break;
        default:
      }
    } else if (this.menuState == MenuState.shopBuy) {
      List<String> indexMenu = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
      if (indexMenu.contains(cmd)) {
        this.buyItem(int.parse(cmd));
      } else if (cmd == "0") {
        this.menuState = MenuState.shop;
      }
    } else if (this.menuState == MenuState.shopSell) {
      List<String> indexMenu = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
      if (indexMenu.contains(cmd)) {
        this.sellItem(int.parse(cmd));
      } else if (cmd == "0") {
        this.menuState = MenuState.shop;
      }
    }
  }

  void nextDay() {
    this.applyDay();
    this._day++;
  }

  void devCmd(List<String> args) async {
    if (_useDevCmd) {
      switch (args[0]) {
        case "/addcoin":
          if (args.length > 1) {
            int? coin = int.tryParse(args[1]);
            if (coin != null) this._character.addCoin(coin);
          }
          break;
        case "/setcoin":
          if (args.length > 1) {
            int? coin = int.tryParse(args[1]);
            if (coin != null) this._character.setCoin(coin);
          }
          break;
        default:
      }
    }
  }

  void openMainMenu() {
    String selected = this._mainMenu.show();
    if (selected == "2") this._run = false;
  }

  String _getGender(Gender gender) {
    return (gender == Gender.Female) ? "Female" : "Male";
  }

  String addChar(String char, int count) {
    // print(count);
    String string = "";
    for (var i = 0; i < count; i++) {
      // print("loop");
      string = string + char;
    }
    return string;
  }

  String format(int width, String string) {
    int textWidth = string.length;
    int spaceWidth = width - textWidth;
    return "${string}${this.addChar(" ", spaceWidth)}";
  }

  /**
    * Black:   \x1B[30m
    * Red:     \x1B[31m
    * Green:   \x1B[32m
    * Yellow:  \x1B[33m
    * Blue:    \x1B[34m
    * Magenta: \x1B[35m
    * Cyan:    \x1B[36m
    * White:   \x1B[37m
    * Reset:   \x1B[0m
   */

  String plantUi(int max, int now, [bool water = false]) {
    if (now == 0) {
      return (water) ? "\x1B[32m•\x1B[0m" : "\x1B[33m•\x1B[0m";
    } else if (now > 0 && now < max) {
      return (water) ? "\x1B[32mW\x1B[0m" : "\x1B[33mW\x1B[0m";
    } else if (now >= max) {
      return "\x1B[32m\*\x1B[0m";
    } else {
      return "-";
    }
  }

  String getPlantUi(int index) {
    Seed? plant = this.getPlotSeed(index);
    if (plant != null) {
      return plantUi(plant.maxDay, plant.state, plant.water);
    } else {
      return " ";
    }
  }

  void ui() {
    String name = _character.name();
    String gender = this._getGender(_character.gender());
    int textWidth =
        "+---------------------------------------------------+".length;
    int genderWidth = name.length;
    int nameWidth = gender.length;
    int addSpace = textWidth - (2 + genderWidth + 1 + nameWidth + 2);
    String characterText =
        "| ${gender}:${name}${this.addChar(" ", addSpace)} |";
    // String characterText = "| ${this.menuState}|";

    int sp = this._character.stamina();
    int spWidth = 4;
    int charSpWidth = sp.toString().length;
    int maxSp = this._character.maxStamina();
    int maxSpWidth = 4;
    int charMaxSpWidth = maxSp.toString().length;
    int spaceSpWidth = spWidth - charSpWidth;
    int spaceMaxSpWidth = maxSpWidth - charMaxSpWidth;
    String spText =
        "${this.addChar(" ", spaceSpWidth + spaceMaxSpWidth)}${sp}/${maxSp}";

    int coin = this._character.coin;
    int coinWidth = 6;
    int charCoinWidth = coin.toString().length;
    int spaceCoinWidth = coinWidth - charCoinWidth;
    String coinText = "${this.addChar(" ", spaceCoinWidth)}${coin}";

    int day = this._day;
    int dayWidth = 3;
    int mapDayWidth = day.toString().length;
    int spaceDayWidth = dayWidth - mapDayWidth;
    String dayText = "${this.addChar(" ", spaceDayWidth)}${day}";

    this.updateMenuList(menuState);
    List<String> menu = this._menuList;

    String helpText = this._helpText;
    int helpTextMaxWidth = 49;
    int helpTextWidth = helpText.length;
    int spacehelpTextWidth = helpTextMaxWidth - helpTextWidth;
    String help = "${helpText}${this.addChar(" ", spacehelpTextWidth)}";

    // String text = sprintf("%08d", 1);
    print('+---------------------------------------------------+');
    print(characterText);
    print('+----------+---------------+---------+--------------+');
    print('| Day: ${dayText} | SP: ${spText} |         | Coin: ${coinText} |');
    print('+----------+------+--------+---------+--------------+');
    print('|  +---+---+---+  | 1. ${format(28, menu[0])} |');
    print(
        '|  | ${getPlantUi(0)} | ${getPlantUi(1)} | ${getPlantUi(2)} |  | 2. ${format(28, menu[1])} |');
    print('|  +---+---+---+  | 3. ${format(28, menu[2])} |');
    print(
        '|  | ${getPlantUi(3)} | ${getPlantUi(4)} | ${getPlantUi(5)} |  | 4. ${format(28, menu[3])} |');
    print('|  +---+---+---+  | 5. ${format(28, menu[4])} |');
    print(
        '|  | ${getPlantUi(6)} | ${getPlantUi(7)} | ${getPlantUi(8)} |  | 6. ${format(28, menu[5])} |');
    print('|  +---+---+---+  | 7. ${format(28, menu[6])} |');
    print('|                 | 8. ${format(28, menu[7])} |');
    print('|                 | 9. ${format(28, menu[8])} |');
    print('|                 | 0. ${format(28, menu[9])} |');
    print('+-----------------+---------------------------------+');
    print('| ${help} |');
    print('| >                                                 |');
    print('+---------------------------------------------------+');
  }
}

class MainProgram {
  CursorUtility CursorUtil = CursorUtility();
  StartMenu startMenu = StartMenu();

  String text = "";
  bool _run = true;

  void start() {
    while (_run) {
      this.CursorUtil.clear();
      String selected = startMenu.show(text);
      if (selected != "1" && selected != "2") {
        this.text = "Please enter number of choice.";
        this.start();
      } else if (selected == "1") {
        this.characterCreate();
        // exit(0);
      } else if (selected == "2") {
        print('Ok bye.');
        this._run = false;
        // exit(0);
      } else {
        print('You select: $selected');
      }
    }
  }

  void characterCreate() {
    this.CursorUtil.clear();
    CharacterCreateMenu createCharacterMenu = CharacterCreateMenu();
    createCharacterMenu.show();
    String name = createCharacterMenu.getName();
    Gender gender = createCharacterMenu.getGender();
    // print("Name: " + name);
    // print("Gender: " + gender.toString());
    Character character = Character(name, gender);
    MainMap mainMap = MainMap(character);
    this.CursorUtil.clear();
    mainMap.run();
  }
}

void main() {
  MainProgram mainProgram = MainProgram();
  mainProgram.start();
}
